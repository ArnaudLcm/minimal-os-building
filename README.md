# Minimal OS Building Tool

## Introduction
This project aims to provide a tool to easily create a minimal operating system with [Alpine Linux](https://www.alpinelinux.org/).

Firstly made for [Kadeploy](https://gitlab.inria.fr/grid5000/kadeploy), you can use it for other purposes. 

Unlike tools such as debootstrap and debirf, you will be able to create minimal operating system regardless of your distribution.

## Requirements
Please check that you have the following dependecies :
- [Ruby](https://www.ruby-lang.org/fr/) >= 2.5
- C Compiler recent enough for the wanted kernel version
- Kernel depedencies for your version : Check this [link](https://www.kernel.org/doc/html/)
- lz4
- cpio

## Supported architectures

Here is the list of supported architectures :
- x86_64
- x86
- aarch64
- armhf
- armv7
- ppc64le
- s390x

## Features
- Possibility to select what you want to build : initrd, kernel or both
- Possibility to launch a custom script : `shared/post.sh` which will be launched at the end of the initrd build.
- Possibility to add a startup script : `shared/startup.sh` which will be launched at the end of the kernel boot. You can for instance start your services through this script.

 ## Usage
In order to use this tool :
```bash
    ruby build.rb
```

After this step, you should have 2 files :
- bzImage : Compiled kernel
- initramfs.img.lz4 : Your initrd

You can now try your image with your own hypervisor. Here is an example for my system :
```bash
qemu-system-x86_64 -kernel bzImage -initrd initramfs.img.lz4
```
**WARNING:** Please take a special attention to the RAM allocated to your VM. If it is not sufficiant, it can leads to unwanted behaviours.

## Configuration

### **Services**
You can preinstall libraries on your mini os.
The init system OpenRC is installed by default.

Moreover, a openssh server and client is installed. Those services are defined in `services.rb`.

**How to add your own ?**

First, find the package's name you are looking to install : [Alpine Packages](https://pkgs.alpinelinux.org/packages).

Next, open `services.rb` and add a new entry to the *SERVICES_LIST* :
```ruby
    SERVICES_LIST = {
        "MY_PACKAGE_NAME" => -> (working_dir) { self.package_name_routine(working_dir) } #Edit routine name
    }
```

You will now need to create this routine :

```ruby
    def self.package_name_routine(working_dir)
        # DO SOMETHING
    end
```

The tool will firstly install your package. Then, you can provide extract configuration through this routine.

The *working_dir* variable contains the temporary path to alpine files.

### **Scripting:**
2 script files are ready to help you in order to customize your image :
- `scripts/post.sh` : This script is started at the end of every services setup. It is in this script that you usually do your networking configuration, etc... An example is provided, feel free to edit it for your requirements.
- `scripts/startup.sh` : This script is started at the end of the initrd step.

## Usage for kadeploy
In order to be usable for your kadeploy server, you need to put in the ssh service folder the key that you are using on your server.
Moreover, you will be need to have a specific `startup.sh` script in order to launch a service which will listen to your test_deployment_mini_os port (Default : 25300).

Here is what I've been using :
```bash
#! /bin/sh

# This script is run by the end of the kernel boot, just after the init script.

ln -s /sbin/busybox /bin/ash
rc-status -a
rc-service networking start
rc-service sshd start
(while true; do nc -l -p 25300; done) &
```



Regarding services, here is a working list : 
```ruby
#!/usr/bin/ruby

module Services

    def self.ssh_routine(working_dir)
        system("mkdir -p #{working_dir}/root/.ssh")
        system("cat #{__dir__}/services/ssh/id_deploy.pub >> #{working_dir}/root/.ssh/authorized_keys")
    end

    def self.busybox_routine(working_dir)
        system("mkdir -p #{working_dir}/etc/network")
        system("cp #{__dir__}/services/busybox-extras/interfaces #{working_dir}/etc/network")
    end

    def self.e2fsprogs_routine(working_dir)
        system("rm -f #{working_dir}/etc/mke2fs.conf 2> /dev/null")
        system("cp #{__dir__}/services/e2fsprogs/mke2fs.conf #{working_dir}/etc/mke2fs.conf")
    end

    SERVICES_LIST = {
        "openssh" => -> (working_dir) { self.ssh_routine(working_dir) },
        "busybox-extras" => -> (working_dir) {self.busybox_routine(working_dir)},
        "nano" => -> (working_dir) {},
        "sudo" => -> (working_dir) {},
        "make" => -> (working_dir) {},
        "perl" => -> (working_dir) {},
        "parted" => -> (working_dir)  {},
        "bash" => -> (working_dir) {},
        "util-linux" => -> (working_dir) {},
        "eudev" => -> (working_dir) {},
        "e2fsprogs" => -> (working_dir) {self.e2fsprogs_routine(working_dir)},
        "ruby" => -> (working_dir) {},
        "ctorrent-dnh" => -> (working_dir) {},
        "kexec-tools" => -> (working_dir) {},
        "tar" => -> (working_dir) {},
        "zstd" => -> (working_dir) {},
        "coreutils" => -> (working_dir) {},
        "e2fsprogs-extra" => -> (working_dir) {},
    }

end
```

with those configurations :

- mke2fs.conf :
```configuration
[defaults]
	base_features = sparse_super,large_file,filetype,resize_inode,dir_index,ext_attr
	default_mntopts = acl,user_xattr
	enable_periodic_fsck = 0
	blocksize = 4096
	inode_size = 256
	inode_ratio = 16384

[fs_types]
	ext3 = {
		features = has_journal
	}
	ext4 = {
		features = has_journal,extent,huge_file,flex_bg,64bit,dir_nlink,extra_isize
	}
	small = {
		blocksize = 1024
		inode_ratio = 4096
	}
	floppy = {
		blocksize = 1024
		inode_ratio = 8192
	}
	big = {
		inode_ratio = 32768
	}
	huge = {
		inode_ratio = 65536
	}
	news = {
		inode_ratio = 4096
	}
	largefile = {
		inode_ratio = 1048576
		blocksize = -1
	}
	largefile4 = {
		inode_ratio = 4194304
		blocksize = -1
	}
	hurd = {
	     blocksize = 4096
	     inode_size = 128
	     warn_y2038_dates = 0
	}

```

Here is some explanations :
- zstd : This is the format commonly used for your images tarball. If it's not your case, you can delete it.
- e2fsprogs : In this package, there is tools such as mkfs in order to format your partitions.
- parted : Tool used to partition disk but you can also use fdisk (check your partitioning script)
- perl : Perl is mandatory to allow taktuk's propagation
- e2fsprogs-extra : Package containing fuse2fs, can be useful in some situations
- eudev: In order to add udev rules
- mke2fs.conf : Some distributions don't support specific ext4 features : metadata_csum_seed and orphan_files (for instance Debian) which are enable by default on e2fsprogs 1.47.0. In this case, you will have to disable them by editing efsprogs configuration file. See https://bugs.launchpad.net/ubuntu/+source/e2fsprogs/+bug/2025339 for futher details.


## Test
This script has been tested with success on the following environnement :
```
Operating system : Arch Linux 6.3.7-arch1-1
CPU : x86_64
```