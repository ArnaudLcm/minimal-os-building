#!/usr/bin/ruby

module Services

    def self.ssh_routine(working_dir)
        system("mkdir -p #{working_dir}/root/.ssh")
        system("cat #{__dir__}/services/ssh/id_deploy.pub >> #{working_dir}/root/.ssh/authorized_keys")
    end

    def self.busybox_routine(working_dir)
        system("mkdir -p #{working_dir}/etc/network")
        system("cp #{__dir__}/services/busybox-extras/interfaces #{working_dir}/etc/network")
    end

    SERVICES_LIST = {
        "openssh" => -> (working_dir) { self.ssh_routine(working_dir) },
        "busybox-extras" => -> (working_dir) {self.busybox_routine(working_dir)},
    }

end
