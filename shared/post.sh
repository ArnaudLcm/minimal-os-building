#!/bin/sh

# Information regarding arguments :
# $1 : Path to the working directory
# $2 : Path to the script directory

head -n 1 $1/etc/passwd |sed -e 's/root/deploy/1' -e 's/root/deploy/1'>> $1/etc/passwd
head -n 1 $1/etc/shadow |sed 's/root/deploy/' >> $1/etc/shadow
head -n 1 $1/etc/shadow- |sed 's/root/deploy/' >> $1/etc/shadow-

mkdir $1/mnt/dest
mkdir $1/rambin
mkdir $1/mnt/tmp
chmod 770 -R tmp

mkdir -p $1/etc/kadeploy3/keys
cp -p $2/services/ssh/* $1/etc/kadeploy3/keys
cp -p $2/services/ssh/* $1/root/.ssh
chmod 700 $1/etc/kadeploy3/keys/*

sudo systemd-nspawn -D $1 /sbin/sh rc-update add