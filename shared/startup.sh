#! /bin/sh

# This script is run by the end of the kernel boot, just after the init script.

for net_dev in `find /sys/class/net/ -type l`; do
    netName=`basename $net_dev`
    ip link set up dev $netName
done

ln -s /sbin/busybox /bin/ash
rc-status -a
rc-service networking start
rc-service sshd start
(while true; do nc -l -p 25300; done) &