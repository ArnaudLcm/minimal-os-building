#! /bin/sh
#
# This script aims to compress the whole initramfs inside an lz4 archive
#

(
    find $1 -printf "%P\0" | # Retrieve all files
    sudo cpio --directory=$1 --null --create --verbose --owner root:root --format=newc 
) | lz4c -l > $2/initramfs.img.lz4