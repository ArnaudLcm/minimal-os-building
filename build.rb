#!/usr/bin/ruby
require "net/http"
require "timeout"
require_relative 'services'

# CONSTANTS
DEBUG_MODE = false
KERNEL_VERSION = "6.4.7"
ALPINE_VER = "3.18.2"

TARBALL_TIMEOUT = 10


ARCH_LIST = ["x86_64", "x86", "aarch64", "armhf", "armv7", "ppc64le", "s390x"]

# UTILS

LINK_BUILDER = {
    "kernel" => -> (major, version) { return "https://cdn.kernel.org/pub/linux/kernel/#{major}/linux-#{version}.tar.xz" },
    "alpine" => -> (major, version, arch) { return "https://dl-cdn.alpinelinux.org/alpine/v#{major}/releases/#{arch}/alpine-minirootfs-#{version}-#{arch}.tar.gz" }
}

def print_info(msg)
    STDOUT.puts("[INFO]: "+msg)
end

def print_error(msg)
    STDERR.puts("[ERROR]:"+msg)
end

def print_warning(msg)
    STDOUT.puts("[WARNING]: "+msg)
end

def debug(msg)
    if DEBUG_MODE
        puts("[DEBUG]: "+msg)
    end
end

def exec(cmd)
    system(cmd)
end

def exec_out(cmd)
    return `#{cmd}`
end
  

def check_repo_validity(url)

    begin
        Timeout.timeout(TARBALL_TIMEOUT) do
            url = URI.parse(url)
            req = Net::HTTP.new(url.host, url.port)
            req.use_ssl = true
            res = req.request_head(url.path)
            return res.code == "200" 
        end
    rescue Timeout::Error
        print_error("Request to retrieve #{url} timed out.")
        exit()
    end        
end

def check_version_format(version) # We can do a minimal check because it's sufficiant
    return version.split(".").length > 1 && version.split(".").length <= 3
end

def get_major_kernel(version)
    return "v"+version.split('.')[0]+".x"
end

def get_major_alpine(version)
    version.split('.')[0]+"."+version.split('.')[1]
end

# CORE

def build_kernel()
    print_info("Preparting to build the linux kernel...")
end

class OSHandyMan

    def initialize()

        print_info("Trying to retrieve your architecture...")
        @arch = exec_out("lscpu | grep 'Architecture' | cut -d':' -f2 | xargs").strip
        puts "[INFO]: Is #{@arch} your architecture ?: [Y/n]"
        response = gets.strip
        if response == "n"
            puts "[INFO]: Please provide your architecture: "
            @arch = gets.strip
        end
        

        if !ARCH_LIST.include?(@arch)
            print_error("Your architecture is not supported.")
            exit
        end

        print_info("What do you want to build ? [1]")
        puts("1) Kernel and initrd")
        puts("2) Only kernel")
        puts("3) Only initrd")
        response = gets.strip

        case response
        when "3"
            @do_initrd = true
        when "2"
            @do_kernel = true
        else
            @do_initrd = true
            @do_kernel = true
        end

        if @do_kernel #Kernel information section
            puts "[INFO]: Please specify the wanted kernel version: [#{KERNEL_VERSION}]"
            kernel_version = gets
            @kernel_v = kernel_version.to_s.strip.empty? ? KERNEL_VERSION : kernel_version.to_s.strip
            
            debug(@kernel_v)
    
            if !check_version_format(@kernel_v)
                print_error("Seems that the provided kernel version is not valid.")
                exit
            end
    
            @kernel_link = LINK_BUILDER["kernel"].call(get_major_kernel(@kernel_v), @kernel_v)
    
            if !check_repo_validity(@kernel_link)
                print_error("Seems that the provided kernel version does not exist.")
                exit
            end
        end

        if @do_initrd #Initrd information section
            puts "[INFO]: Please specify the wanted alpine version: [#{ALPINE_VER}]"
            alpine_version = gets
            @alpine_v = alpine_version.to_s.strip.empty? ? ALPINE_VER : alpine_version.to_s.strip
            
            debug(@alpine_v)
    
            if !check_version_format(@alpine_v)
                print_error("Seems that the provided alpine version is not valid.")
                exit
            end
    
            @alpine_link = LINK_BUILDER["alpine"].call(get_major_alpine(@alpine_v), @alpine_v, @arch)
            if !check_repo_validity(@alpine_link)
                print_error("Seems that the provided alpine version does not exist.")
                exit
            end
        end
        exec("mkdir -p output")  
    end

    def build_kernel()
        if @do_kernel
            printf("Creating the working directory")
            @working_dir = exec_out("mktemp -d").strip
            debug(@working_dir)
            print_info("Retrieving kernel tarball")
            exec("wget -c #{@kernel_link} --directory-prefix=#{@working_dir}")
            print_info("Extracting tarball")
            exec("tar xvf #{@working_dir}/linux-#{@kernel_v}.tar.xz -C #{@working_dir}")
            exec("cd #{@working_dir}/linux-#{@kernel_v}")
            print_info("Cleaning files...")
            exec("cd #{@working_dir}/linux-#{@kernel_v}; make mrproper")
            print_info("The kernel is now ready to be compiled")
            exec("cd #{@working_dir}/linux-#{@kernel_v}; make defconfig; make -j$(nproc)") #@TODO: May be find a better way to always have to cd
            print_info("Kernel has been compiled")
            puts "[INFO]: Do you want to install modules ?: [y/N]"
            response = gets.strip
            if response == "y"
                @add_modules = True # @TODO: Directly add it within in the initrd
                exec("cd #{@working_dir}/linux-#{@kernel_v}; sudo make modules")
                exec("cd #{@working_dir}/linux-#{@kernel_v}; INSTALL_MOD_PATH=#{@working_dir}/linux-#{@kernel_v} sudo make modules_install")
            end

            if !File.exist?("#{@working_dir}/linux-#{@kernel_v}/arch/#{@arch}/boot/bzImage")
                print_info("Seems like bzImage has not been already created, let's do it.")
                exec("cd #{@working_dir}/linux-#{@kernel_v}; sudo make bzImage")
            end
            exec("cp #{@working_dir}/linux-#{@kernel_v}/arch/#{@arch}/boot/bzImage #{__dir__}/output")
            exec("sudo rm -rf #{@working_dir}")
            print_info("Kernel step is finished !")

        end
    end

    def build_rootfs()
        if @do_initrd
            printf("Let's now handle initrd part")
            printf("Creating the working directory")
            @working_dir = exec_out("mktemp -d").strip
            debug(@working_dir)
            print_info("Retrieving alpine tarball")
            exec("wget -c #{@alpine_link} --directory-prefix=#{@working_dir}")
            print_info("Extracting tarball")
            exec("tar xvf #{@working_dir}/alpine-minirootfs-#{@alpine_v}-#{@arch}.tar.gz -C #{@working_dir}")
            exec("rm #{@working_dir}/alpine-minirootfs-#{@alpine_v}-#{@arch}.tar.gz")
            print_info("Building init exec")
    
            init_content = <<-EOF
#! /bin/sh
#
# /init executable file in the initramfs 
#
mount -t devtmpfs dev /dev
mkdir /dev/pts
mount -t devpts /dev/pts /dev/pts
mount -t proc proc /proc
mount -t sysfs sysfs /sys
/root/startup.sh
exec /sbin/getty -n -l /bin/sh 115200 /dev/console
poweroff -f
EOF
    
            init_file = File.new("#{@working_dir}/init",  "w+")
            init_file.write(init_content)
            init_file.close
            exec("chmod +x #{@working_dir}/init")
            exec("cp #{__dir__}/shared/startup.sh #{@working_dir}/root")
            exec("chmod +x #{@working_dir}/root/startup.sh")
            
            exec("sudo systemd-nspawn -D #{@working_dir} /sbin/apk add openrc --update-cache")
            system("mkdir #{@working_dir}/run/openrc")
            system("touch #{@working_dir}/run/openrc/softlevel")
            print_info("Post installation script..")
            exec("sudo chmod +x #{__dir__}/shared/post.sh")
            exec("#{__dir__}/shared/post.sh #{@working_dir} #{__dir__}")

            print_info("Starting to install services")
            Services::SERVICES_LIST.each do |key, routine|
                print_info("Installing #{key}")
                exec("sudo systemd-nspawn -D #{@working_dir} /sbin/apk add #{key} --update-cache")
                routine.call(@working_dir)
            end
            print_info("Building img...")
            
            exec("sudo chmod +x #{__dir__}/shared/build_image.sh")

            exec("#{__dir__}/shared/build_image.sh #{@working_dir} #{__dir__}/output")
            print_info("Initrd step is a success.")
            print_info("Cleaning...")
            exec("sudo rm -rf #{@working_dir}")
            print_info("Your image should be available in the output folder.")

        end


    end

end

handy_man = OSHandyMan.new
handy_man.build_kernel
handy_man.build_rootfs